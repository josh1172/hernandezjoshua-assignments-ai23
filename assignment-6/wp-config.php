<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'joshua' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'MFkd,7a`X$Iah;]v3/Fk[(NXvDmY|vCy82&|hyw?1q8c,8$n6iU+KeY[n=}j=;9I' );
define( 'SECURE_AUTH_KEY',  'syW^5W,#?I%7[Aq_|r^Uq[Uk$2Z_MW<kLnJ}$~RMTIvMNLw^:5nN6=TwArQ8Lb%8' );
define( 'LOGGED_IN_KEY',    'A3tVR7O.>VGnJc)}mtlAc8PHfG*7$WJ,Ps!i</&$#In1BZ ([ d`e.q-;Z5Isb|Y' );
define( 'NONCE_KEY',        'bBi)EX<Ynf>)OGt[kQeVeCOgI k:F{=Hn.R]jO@|%~+_3zVkz9mBM;zqNrX=M}S|' );
define( 'AUTH_SALT',        'BR5p^u!k:8QY&{c|Wm!8]0N%)je{<PaAxF|6^aT|yt4fg#+m~OCl&/O#DtZr3`}8' );
define( 'SECURE_AUTH_SALT', 'AWHn,uof[lcMfvnuE.todhWWPPILh$RO=yH/+{O-G=SWA;u/NTKu}#j0#Ug]- nG' );
define( 'LOGGED_IN_SALT',   '9^cDE h @* ,7(5i,w3Ma_&S5$=2lxtKfz*J9h|_aS~yHISAVYuX!,L9k0B05]Kj' );
define( 'NONCE_SALT',       '.}]!#JHCrWiDgAx:KpvEeOX/M891R<mN)KZnJ6jA%qYh]`mr&b-Yko-U0#7Ys1@@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
